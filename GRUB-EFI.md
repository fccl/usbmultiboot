Plusieurs tentative de construire une clé Hybid multiboot ne donnent pas satisfaction. Le nombre de machine non EFI se restreint beaucoup aussi je choisi de me concentrer uniquement sur la solution EFI beaucoup plus simple à mettre en oeuvre.

# Prérequis
Pour faire une clé EFI, il faut un volume (ou Device) avec une table de partition [GUID](https://fr.wikipedia.org/wiki/GUID_Partition_Table) souvent appelé GPT pour GUID Partition Table. Sur ce volume, il faut une partition Fat32 avec un fanions ESP (EFI System Partition qui entraine automatiquement le fanion boot). Il y a des limites associées au Fat32 (notamment pas de fichier de plus de 4Go) mais pour des clé USB de 8 ou 16Go cela ne pose pas trop de problème.

# Installation
Le script **InstallEFI.sh** s'occupe donc de préparer le volume (souvent une clé USB de 8Go par exemple), d'y créer une partition Fat32 avec le fanion ESP, et de la formater. Ensuite il monte cette partition dans un dossier créé à cette effet dans le dossier du script et construit un dossier *EFI/BOOT* nécessaire à l'installation de GRUB x86_64-efi. Enfin il crée un un fichier grub.cfg dans le dossier boot/grub avec les éléements nécessaire aux menus de boot.

A la fin, le script ne se termine pas sans un appuis sur une touche afin de vous laisser le temps d'inspecter ce qui a été créé dans le dossier utilisé pour monter la partition.

# Remarque
  * Le projet Tetris ne semble plus/pas fonctionner.
  * Il y a u problème avec le loopback qui fait un out of memory. Donc le fonctionnement multiboot ne fonctionne pas. En revanche le fonctionne de GRUB-efi est bon.


