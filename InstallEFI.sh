#!/bin/sh
#on s'assure d'être en mode administrateur
if [ 'root' != $USER ] ; then
  echo "le script doit avoir les droits root"
  exit 1
fi
if [ $# != "1" ]; then
  echo "usage : ./prepareMultiBoot.sh /dev/x ou x est le nom du Disk parmi ceux listé ci-dessous"
  fdisk -l
  exit 1
fi
DEVICE=$1
MOUNT_POINT=usbKey
partitionEFI=1
sizeEfi=14000MiB
partitionExt4=2
mountEfi=$MOUNT_POINT/esp
mountExt4=$MOUNT_POINT/persiste

# Démonte les éventuelles partitions que seraient montées
umount ${DEVICE}?*
#umount /dev/loop*
#losetup -d /dev/loop*

echo "-------- Nettoyage de la clé wipefs-----------"
wipefs -a $DEVICE
if [ $? != "0" ]; then
  echo "Accès au média impossible"
  exit 1
fi
echo "-------- Nettoyage de la clé sgdisk-----------"
sgdisk -Z $DEVICE
echo "-------- Défini un table de partition compatible EFI"
parted -s $DEVICE mklabel gpt
if [ $? != "0" ]; then
  echo "impossible de position la table de partition"
  exit 1
fi
echo "-------- Création d'une partition fat32 avec le fanion esp"
parted -s $DEVICE mkpart primary fat32 2048s $sizeEfi
parted -s $DEVICE set $partitionEFI esp on

echo "-------- format Fat32 (efi)"
mkfs.fat -F32 ${DEVICE}$partitionEFI
rm -rf $MOUNT_POINT
mkdir -p $mountEfi $mountExt4
mount ${DEVICE}$partitionEFI $mountEfi
mkdir -p $mountEfi/EFI/BOOT
# apt update
# apt install grub-efi-amd64
echo "-------- install grub efi"
grub-install --target=x86_64-efi --recheck --removable --efi-directory=${mountEfi} --boot-directory=${mountEfi}/boot
rm $mountEfi/EFI/BOOT/BOOTX64.EFI $mountEfi/EFI/BOOT/BOOTX64.CSV
cp $mountEfi/EFI/BOOT/grubx64.efi $mountEfi/EFI/BOOT/BOOTX64.EFI

echo "-------- Création d'une partition Ext4 pour une copie d'un OS"
parted -s $DEVICE mkpart primary ext4 $sizeEfi 100%
echo "y" | mkfs.ext4 -L persistence ${DEVICE}$partitionExt4
echo "montage des partition loop et 2"
mount ${DEVICE}$partitionExt4 $mountExt4
echo "/ union" > $mountExt4/persistence.conf
echo "demonte la partition Ext4"
umount $mountExt4

echo "-------- config grub.cfg pour efi"
#blkid
# uuid=$(blkid | grep ${DEVICE}1 | cut -d '=' -f2)
# echo "uuid == $uuid"
cat >$mountEfi/boot/grub/grub.cfg <<EOF
# grub.cfg
EOF

# . ./LiveMint.sh $mountEfi
# https://linuxmint.com/download_lmde.php
name="lmde6.iso"
echo "Copie de l'iso $name"
cp isos/$name $mountEfi

echo "prepare grub pour $name"
cat >>$mountEfi/boot/grub/grub.cfg <<EOF

menuentry '$name azerty' {
  rmmod tpm
  echo 'prepare loopback'
  loopback loop /$name
  echo 'Loading Linux (vmlinuz)'
  linux (loop)/casper/vmlinuz boot=live findiso=/$name iso-scan/filename=/$name locales=fr_FR.UTF-8 keyboard-layouts=fr persistence
  echo 'Loading initial ramdisk ...'
  initrd (loop)/casper/initrd.lz
}

menuentry '$name Mac-azerty' {
  rmmod tpm
  echo 'prepare loopback'
  loopback loop /$name
  echo 'Loading Linux (vmlinuz)'
  linux (loop)/casper/vmlinuz boot=live findiso=/$name iso-scan/filename=/$name locales=fr_FR.UTF-8 keyboard-layouts=fr keyboard-variants=mac keyboard-options=lv3:switch,compose:lwin persistence
  echo 'Loading initial ramdisk ...'
  initrd (loop)/casper/initrd.lz
}

EOF

# # . ./LiveDebian.sh $mountEfi
# # https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/
# name="z_debian.iso"
# echo "Copie de l'iso $name"
# cp isos/$name $mountEfi

# echo "prepare grub pour $name"
# cat >>$mountEfi/boot/grub/grub.cfg <<EOF

# menuentry '$name azerty' {
#   rmmod tpm
#   echo 'prepare loopback'
#   loopback loop /$name
#   echo 'Loading Linux (vmlinuz)'
#   linux (loop)/live/vmlinuz boot=live findiso=/$name iso-scan/filename=/$name locales=fr_FR.UTF-8 keyboard-layouts=fr persistence
#   echo 'Loading initial ramdisk ...'
#   initrd (loop)/live/initrd.img
# }

# menuentry '$name Mac-azerty' {
#   rmmod tpm
#   echo 'prepare loopback'
#   loopback loop /$name
#   echo 'Loading Linux (vmlinuz)'
#   linux (loop)/live/vmlinuz boot=live findiso=/$name iso-scan/filename=/$name locales=fr_FR.UTF-8 keyboard-layouts=fr keyboard-variants=mac keyboard-options=lv3:switch,compose:lwin persistence
#   echo 'Loading initial ramdisk ...'
#   initrd (loop)/live/initrd.img
# }

# EOF

name="debian-live-12.8.0-amd64-xfce.iso"
echo "Copie de l'iso $name"
cp isos/$name $mountEfi

echo "prepare grub pour $name"
cat >>$mountEfi/boot/grub/grub.cfg <<EOF

menuentry '$name azerty' {
  rmmod tpm
  loopback loop /$name
  linux (loop)/live/vmlinuz boot=live findiso=/$name iso-scan/filename=/$name locales=fr_FR.UTF-8 keyboard-layouts=fr persistence
  initrd (loop)/live/initrd.img
}

menuentry '$name Mac-azerty' {
  rmmod tpm
  loopback loop /$name
  linux (loop)/live/vmlinuz boot=live findiso=/$name iso-scan/filename=/$name locales=fr_FR.UTF-8 keyboard-layouts=fr keyboard-variants=mac keyboard-options=lv3:switch,compose:lwin persistence
  initrd (loop)/live/initrd.img
}

EOF

# name="debian-live-12.8.0-amd64-gnome.iso"
# echo "Copie de l'iso $name"
# cp isos/$name $mountEfi

# echo "prepare grub pour $name"
# cat >>$mountEfi/boot/grub/grub.cfg <<EOF

# menuentry '$name azerty' {
#   rmmod tpm
#   loopback loop /$name
#   linux (loop)/live/vmlinuz boot=live findiso=/$name iso-scan/filename=/$name locales=fr_FR.UTF-8 keyboard-layouts=fr persistence
#   initrd (loop)/live/initrd.img
# }

# menuentry '$name Mac-azerty' {
#   rmmod tpm
#   loopback loop /$name
#   linux (loop)/live/vmlinuz boot=live findiso=/$name iso-scan/filename=/$name locales=fr_FR.UTF-8 keyboard-layouts=fr keyboard-variants=mac keyboard-options=lv3:switch,compose:lwin persistence
#   initrd (loop)/live/initrd.img
# }

# EOF

name="ubuntu-24.04.1-desktop-amd64.iso"
echo "Copie de l'iso $name"
cp isos/$name $mountEfi

echo "prepare grub pour $name"
cat >>$mountEfi/boot/grub/grub.cfg <<EOF

menuentry '$name azerty' {
  rmmod tpm
  loopback loop /$name
  linux (loop)/casper/vmlinuz iso-scan/filename=/$name keyboard-layouts=fr locale=fr_FR bootkbd=fr console-setup/layoutcode=fr
  initrd (loop)/casper/initrd.img
}

menuentry '$name Mac-azerty' {
  rmmod tpm
  loopback loop /$name
  linux (loop)/casper/vmlinuz iso-scan/filename=/$name locales=fr_FR.UTF-8 keyboard-layouts=fr keyboard-variants=mac keyboard-options=lv3:switch,compose:lwin
  initrd (loop)/casper/initrd.img
}

EOF

# echo "-------- install tetris"
# name=tetris.efi
# url=https://github.com/tsani/tetrefis/releases/download/v1.0/$name
# if [ ! -e $mountEfi/$name ] ; then
#   wget -O $mountEfi/$name $url
# fi
# menuentry 'Tetris EFI' {
#   chainloader (\$root)/tetris.efi
# }

cat >>$mountEfi/boot/grub/grub.cfg <<EOF

submenu "GRUB2 options ->" {

  menuentry "GRUB Bootloader - variables" {
    echo "Bienvenue sur votre clé multiboot!"
    echo "prefix : \$prefix"
    echo "root : \$root"
    echo "cmdpath : \$cmdpath"
    echo "grub_platform : \$grub_platform"
    read
  }

  menuentry 'List devices/partitions' {
    ls -l
    read
  }
  menuentry 'List (hd0,gpt1)' {
    echo "ls (hd0,gpt1)/"
    ls (hd0,gpt1)/
    echo "ls (hd0,gpt1)/efi/"
    ls (hd0,gpt1)/efi/
    read
  }
  menuentry 'List (hd0,gpt2)' {
    echo "ls (hd0,gpt2)/"
    ls (hd0,gpt2)/
    echo "ls (hd0,gpt2)/efi/"
    ls (hd0,gpt2)/efi/
    read
  }
  menuentry 'List (hd0,gpt3)' {
    echo "ls (hd0,gpt3)/"
    ls (hd0,gpt3)/
    echo "ls (hd0,gpt3)/boot/grub/"
    ls (hd0,gpt3)/boot/grub/
    read
  }
}

menuentry "Reboot" {
  echo "System rebooting..."
  reboot
}

menuentry "Shutdown" {
  echo "System shutting down..."
  halt
}

EOF

# update-grub

read -p "Continue pour démonter la partition et supprimer usbKey/" confirme
echo "Démonte la partition EFI"
umount $mountEfi
echo "Nettoyage des dossiers de travail"
rm -rf $MOUNT_POINT
