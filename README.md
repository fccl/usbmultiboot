# usbMultiboot
Objectif : créer des clés bootable avec différente iso pour tester différents OS ou avoir une clé d'intervention de secours.
L'otion initiale est d'avoir une clé bootable Hybrid (EFI-Legacy).Cependant l'expérience montre que ce n'est pas si simple. Aussi, on se concentrera dorenavent sur l'EFI qui est dvenu la norme.

##  Fonctionnement
Le script pour installer un clé bootable efi est : *InstallEFI.sh*. La documentation associée à ce script est dans le fichier [GRUB-EFI.md](./GRUB-EFI.md).

~~Le script principal est **prepareUSBMultiboot.sh**. Il doit être utilisé en tant que root et nécessite en paramètre le chemin vers le device sur lequel doit être installé grub et les iso à charger.~~

~~Le script makeGrub.bash génère le menu à partir des scripts avec l'extension *.grub.sh*. Ce script peut être lancé indépendamment pour permettre de faire des tests. Sinon il est appelé par le script principal pour charger la clé. Chaque script *.grub.sh* ajoute les données au fichier **boot/grub/grub.cfg**.~~

## Documentation
De la documentation complémentaire sur le fonctionement et les commande de GRUB peut être trouvée dans le dossier [doc_GRUB](./doc_GRUB/grub.md)

## Liens interressants

  * https://emmabuntus.org
  * https://github.com/mytbk/liveusb-builder
  * [Tetris.efi](https://github.com/tsani/tetrefis)
  * [MacPro 1,1 : Installer Linux Mint avec amorçage EFI ](https://pila.fr/wordpress/?p=1511) - blkid
  * [Amorçage UEFI](https://papy-tux.legtux.org/doc1247/index.html)
  * [Ventoy](https://www.ventoy.net/en/index.html)
  * [YUMI4Linux](https://yumiusb.com/)
  * [Boot Multiple ISO from USB via GRUB2](https://pendrivelinux.com/boot-multiple-iso-from-usb-via-grub2-using-linux/)