# Comment créer une clé USB multiboot avec Linux
Ce texte et ce projet ont été initialement trouvé sur https://fr.linux-console.net/?p=11070

## Objectif
Créez un périphérique USB amorçable contenant plusieurs distributions Linux.

## Exigences
  * Un périphérique USB avec une taille suffisante pour contenir plusieurs isos
  * Autorisations root pour modifier les partitions du périphérique et installer grub

## Introduction
Pouvoir exécuter un système d'exploitation complet directement à partir d'un support d'installation est extrêmement utile : nous pouvons tester une distribution, l'utiliser à des fins de sauvegarde ou peut-être pour réparer un système existant. La manière habituelle de créer un support de démarrage consiste à y écrire une image système à l'aide de la commande dd ou d'un outil dédié. Dans ce tutoriel, nous verrons comment créer un périphérique USB multiboot pouvant héberger plusieurs images de distribution.

## Un mot sur BIOS vs UEFI
UEFI (Unified Extensible Firmware Interface), il s'agit du micrologiciel moderne créé en remplacement de l'ancien BIOS (Basic Input Output System). La majorité des distributions Linux récentes peuvent démarrer dans les deux modes sans problème : l'installateur ajustera automatiquement son comportement en conséquence.

Les deux firmwares sont, par défaut, associés à une disposition de table de partition spécifique : UEFI va de pair avec gpt, tandis que BIOS est livré avec le msdos un. Ces associations ne sont cependant pas strictement obligatoires, puisque, du moins en théorie, un micrologiciel UEFI peut démarrer le processus de démarrage à partir d'une configuration de disque msdos et d'un ancien BIOS. Le système peut faire la même chose à partir d'un disque partitionné gpt.

Dans le premier cas, la partition EFI doit être la première sur le disque et être formatée avec le système de fichiers fat32, dans le second, un boot du bios sans aucun système de fichiers devrait exister, nécessaire pour stocker l'étape grub 1.5, car sur une disposition gpt l'écart entre l'enregistrement de démarrage principal et la première partition n'existe pas (c'est là que cette étape grub est généralement installée).

Nous préparerons notre appareil en utilisant une disposition msdos traditionnelle, en installant le chargeur de démarrage grub en mode efi et Legacy pour pouvoir démarrer les distributions à la fois dans UEFI et BIOS. mode .

## Préparation de l'appareil
La première chose que nous devons faire est de préparer notre appareil. Nous allons créer deux partitions, dans l'ordre :

  * Une partition EFI
  * Une partition de données

La première est nécessaire pour démarrer en mode UEFI, puisque c'est là que grub-efi est installé. La partition doit être formatée avec un système de fichiers fat32. Théoriquement, l'installation fonctionnerait même avec cette seule partition, puisque nous pourrions également l'utiliser pour stocker les images de distribution que nous souhaitons utiliser. Cependant, dans ce cas, nous serions limités, puisque fat32 ne prend pas en charge les fichiers de plus de 4 Go, et certains iso peuvent dépasser cette taille.

La deuxième partition sera formatée avec un système de fichiers pris en charge par grub et hébergera les images et les fichiers de configuration de grub.

Pour les besoins de ce didacticiel, je suppose que le périphérique que nous voulons préparer est /dev/sdb. Pour trouver le bon périphérque il faut executer la commande :

    sudo fidk -l        # sous linux
    sudo diskutil list  # sur OSX

## La table de partition et la partition EFI
La première chose à faire est de créer une table de partition msdos sur le périphérique :

    parted -s /dev/sdb mklabel msdos

Après cette étape, nous pouvons créer la partition EFI et la formater avec un système de fichiers fat32. La taille recommandée pour la partition est 550 Mio : sur des partitions plus petites, nous pourrions recevoir une erreur telle que "pas assez de clusters pour une FAT 32 bits" :

    parted -s /dev/sdb mkpart primary 1MiB 551MiB

Nous procéderons ensuite à l'activation des drapeaux esp et boot :

    parted -s /dev/sdb set 1 esp on
    parted -s /dev/sdb set 1 boot on

Enfin nous devons créer le système de fichiers fat32 :

    mkfs.fat -F32 /dev/sdb1

## La partition de données
L'autre partition que nous devons créer est la partition de données, qui hébergera les images ISO des distributions et les fichiers de configuration grub. Nous pouvons ajuster la taille de la partition à nos besoins : plus elle est grande, plus elle pourra contenir d'images. Ici, nous utiliserons tout l'espace restant sur l'appareil :

    parted -s /dev/sdb mkpart primary 551MiB 100%

Nous pouvons formater la partition avec l'un des systèmes de fichiers pris en charge par grub. Dans ce cas, j'utiliserai ext4 :

    mkfs.ext4 /dev/sdb2

## Créez les points de montage et montez les partitions
L'étape suivante consiste à monter la partition EFI et la partition de données quelque part dans notre système afin que nous puissions y créer les répertoires nécessaires, installer grub et mettre nos fichiers iso en place :

    # Create the mountpoints
    mkdir /media/{efi,data}

    # Mount the EFI partition
    mount /dev/sdb1 /media/efi

    # Mount the data partition
    mount /dev/sdb2 /media/data

## Installation du chargeur de démarrage grub
Pour que notre appareil puisse fonctionner à la fois en modes legacy et UEFI, nous devons installer grub et grub pour efi. Sur Fedora, le binaire grub s'appelle grub2 et pour générer une configuration personnalisée sur une configuration UEFI, l'installation du package grub2-efi-modules est également nécessaire. Sur certaines autres distributions, le nom du binaire est simplement « grub » :

    # Installing legacy grub2
    sudo grub2-install \
      --target=i386-pc \
      --recheck \
      --boot-directory="/media/data/boot" /dev/sdb

    # Installing grub for efi
    sudo grub2-install \
      --target=x86_64-efi \
      --recheck \
      --removable \
      --efi-directory="/media/efi" \
      --boot-directory="/media/data/boot"

Comme vous pouvez le constater, dans les deux cas, nous avons utilisé /media/data/boot comme répertoire de démarrage grub. Ce répertoire sera automatiquement créé au lancement des commandes ci-dessus et hébergera les fichiers de configuration grub.

## Copiez les images et créez le fichier grub.cfg
Pour créer ou mettre à jour une configuration Grub sur un système Linux moderne, nous exécutons généralement la commande grub2-mkconfig ou, dans les distributions basées sur Debian, le script wrapper update-grub . Ces outils effectuent automatiquement les opérations nécessaires.

Dans notre cas, cependant, nous devons mettre la main à la pâte et créer la configuration manuellement. Toutes les distributions ne nécessitent pas les mêmes directives, mais nous verrons ici quelques exemples courants. Nous devons d'abord créer le répertoire qui hébergera les images de nos distributions :

    mkdir /media/data/boot/iso

En option, nous pouvons vouloir nous approprier ce répertoire, pour pouvoir y déplacer des images plus facilement. En supposant un id et un gid de 1000, nous exécuterions :

    chown 1000:1000 /media/data/boot/iso

Supposons maintenant que nous souhaitions inclure la dernière image netinstall de Fedora dans notre configuration. Nous devons d'abord télécharger l'image dans le répertoire /media/data/iso :

    wget -O /media/data/boot/iso/Fedora-Workstation-netinst-x86_64-28-1.1.iso https://download.fedoraproject.org/pub/fedora/linux/releases/28/Workstation/x86_64/iso/Fedora-Workstation-netinst-x86_64-28-1.1.iso

Ensuite, une fois le téléchargement terminé, nous devons ajouter une entrée pour celui-ci dans le fichier grub.cfg du répertoire /media/data/boot/grub2. Puisqu'il s'agit de la première entrée que nous ajoutons, le fichier lui-même doit être créé :

    menuentry "Fedora-Workstation-netinst-x86_64-28-1.1" {
        isofile="/boot/iso/Fedora-Workstation-netinst-x86_64-28-1.1.iso"
        loopback loop "${isofile}"
        linux (loop)/isolinux/vmlinuz iso-scan/filename="${isofile}" inst.stage2=hd:LABEL=Fedora-WS-dvd-x86_64-28 quiet
        initrd (loop)/isolinux/initrd.img
    }

La structure des entrées est assez simple : nous avons défini la variable isofile, avec le chemin d'accès à notre image comme valeur. Grub est capable de démarrer à partir d'un répertoire iso, et c'est à cela que sert la commande loopback : dans l'exemple ci-dessus, l'image du système de fichiers sera montée et rendue disponible sur le périphérique loop .

La ligne commençant par Linux contient le chemin d'accès à vmlinuz, qui est l'exécutable du noyau, ainsi que d'autres directives de démarrage. Parmi eux, vous pouvez remarquer iso-scan/filename qui est nécessaire pour trouver l'image iso sur le disque en utilisant le chemin spécifié.

Enfin, la ligne commençant par initrd spécifie le chemin d'accès au initrd. Tous ces chemins sont évidemment relatifs au périphérique de boucle, c'est pourquoi ils sont préfixés par la notation (loop).

Comment connaître les directives que nous devons utiliser dans un cas spécifique ? Une solution consiste à monter l'image quelque part dans notre système et à jeter un œil aux fichiers placés dans le répertoire isolinux. Cependant, des configurations pré-écrites existent déjà et sont faciles à trouver en ligne.

Un autre exemple, maintenant : nous allons ajouter la dernière image de la version Ubuntu à notre périphérique de démarrage :

    wget http://releases.ubuntu.com/18.10/ubuntu-18.10-desktop-amd64.iso?_ga=2.232120863.1006005768.1540658570-734439277.1540658570 \
    -O /media/data/boot/iso/ubuntu-18.10-desktop-amd64.iso

Maintenant, nous ajoutons l'entrée au fichier de configuration :

    menuentry "Ubuntu 18.10 - Try without installing" {
        isofile="/boot/iso/ubuntu-18.10-desktop-amd64.iso"
        loopback loop "${isofile}"
        linux (loop)/casper/vmlinuz iso-scan/filename="${isofile}" boot=casper quiet splash ---
        initrd (loop)/casper/initrd
    }

Voici à quoi devrait ressembler notre menu multiboot à ce stade :

Menu Grub multi-démarrage

## Pensées finales
Dans ce tutoriel, nous avons vu comment créer un périphérique USB multiboot contenant de nombreuses images de distributions. Suite à cette configuration, nous pourrons démarrer les deux sur l'ancien firmware UEFI, en choisissant quelle image lancer dans le menu grub.

Le La configuration de grub.cfg présentée ici est absolument minimale et peut être personnalisée davantage pour inclure de nombreux autres modules et ajuster d'autres paramètres comme le délai d'attente de grub : consulter la documentation officielle de grub est le moyen idéal pour commencer à explorer les nombreuses possibilités.

## Tetris EFI
https://github.com/tsani/tetrefis
https://github.com/tsani/tetrefis/releases

menuentry "LMDE 5 64bit" --class 'os-icon' {
  set isofile='/userprofile/.iso/lmde5-64bit.iso'
  loopback loop (hd0,4)${isofile}
  linux (loop)/live/vmlinuz boot=live iso-scan/filename=${isofile} quiet splash noeject noprompt toram
  initrd (loop)/live/initrd.lz
}

search.fs_uuid 19f17894-f00d-4379-a0f6-60aa1d496681 root hd1,msdos2 
set prefix=($root)'/boot/grub'
configfile $prefix/grub.cfg

