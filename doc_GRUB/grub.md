# Documentation GRUB

## Installation


## Demarrage
Lorsque GRUB présente un menu, il est possible de passe en mode shell avec la touche *c*. De là, plusieurs commandes peuvent être itilisees.

### ls
La commande *ls* seule permet de déterminer l'ensemble des volumes (Device) accessibles. On trouve généralement la liste suivante :

  * (proc) 
  * (memdisk)
  * (hd0)
  * (hd0,gp3)
  * (hd0,gpt2)
  * (hd0,gpt1)
  * (hd1)
  * (hd1,gp3)
  * (hd1,gpt2)
  * (hd1,gpt1)
  * (hd2)

Sur un MacBookPro on truvera : 

  * (proc) 
  * (memdisk)
  * (hd0)
  * (hd0,apple2)
  * (hd0,apple1)
  * (hd0,msdos2)
  * (hd1)
  * (hd2)
  * (hd2,gp3)
  * (hd2,gpt2)
  * (hd2,gpt1)
  * (cdO)

La commande *ls -l [volume]* permet d'avoir accès au détail des volumes.

  * Device proc: Filesystem type procfs - Sector size 512B - Total size 0KiB
  * Device memdisk: Filesystem type fat, UUID 1BEC-56AD - Sector size 512B - Total size 2402KiB
  * Device hd0: Filesystem type iso9660 - Label 'LMDE6....' - Last modification...., UUID 2023-09-22-16-26-06-00 - Sector size 512B - Total size 7 786 496KiB
  * (hd0,apple2)
  * (hd0,apple1)
  * (hd0,msdos2)
  * Device hd1: No known filesystem detected - Sector size 512B - Total size 0.5KiB
  * Device hd2: No known filesystem detected - Sector size 512B - total size 250 059 096B *disque DD du portable de 250M ???*
    * Partition hd2,gp3: Filesystem type ext* - Last modification time xxx, UUID 79d1... - Partition start at 6 351 872KiB - Total size 243 706 880KiB
    * Partion hd2,gpt2: No known filesystem detected - Partition start at 295936KiB - Total size 6 054 912KiB
    * Partition hd2,gpt1: Filesystem type fat, UUID 295F-235F - Partion start ar 2048KiB - Total size 292864KiB 
  * Device cdO: No known filesystem detected - Sector size 2048B - Total size 2KiB
