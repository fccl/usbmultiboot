
iso_folder=$1
mountPoint=$2

name="debian-live-10.8.0-amd64-xfce.iso"

url=https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/$name
if [ ! -e $iso_folder/$name ] ; then
  wget -O $iso_folder/$name $url
fi

if [ -e $iso_folder/$name ] && [ -s $iso_folder/$name ] && [ ! -e $mountPoint/iso/$name ] ; then
  cp $iso_folder/$name $mountPoint/iso
fi

cat <<EOF >> $mountPoint/boot/grub/grub.cfg
submenu 'Debian 10.8 ->' {
  
  menuentry 'Mac-azerty' {
    set isofile="/iso/$name"
    echo 'prepare loopback'
    loopback loop \$isofile
    echo 'Loading Linux (vmlinuz)'
    linux (loop)/live/vmlinuz-4.19.0-14-amd64 boot=live findiso=\$isofile keyboard-layouts=fr keyboard-variant=mac keyboard-options=lv3:switch,compose:lwin locales=fr_FR.UTF-8 
    echo 'Loading initial ramdisk ...'
    initrd (loop)/live/initrd.img-4.19.0-14-amd64
  }

  menuentry 'PC-azerty' {
    set isofile="/iso/$name"
    echo 'prepare loopback'
    loopback loop \$isofile
    echo 'Loading Linux (vmlinuz)'
    linux (loop)/live/vmlinuz-4.19.0-14-amd64 boot=live findiso=\$isofile keyboard-layouts=fr locales=fr_FR.UTF-8
    echo 'Loading initial ramdisk ...'
    initrd (loop)/live/initrd.img-4.19.0-14-amd64
  }

  menuentry 'PC-qwerty' {
    set isofile="/iso/$name"
    echo 'prepare loopback'
    loopback loop \$isofile
    echo 'Loading Linux (vmlinuz)'
    linux (loop)/live/vmlinuz-4.19.0-14-amd64 boot=live findiso=\$isofile locales=fr_FR.UTF-8
    echo 'Loading initial ramdisk ...'
    initrd (loop)/live/initrd.img-4.19.0-14-amd64
  }

}
EOF