
iso_folder=$1
mountPoint=$2

name="lmde-4-cinnamon-64bit.iso"
url=https://ftp.igh.cnrs.fr/pub/linuxmint-iso/debian/$name

if [ ! -e $iso_folder/$name ] ; then
  wget -O $iso_folder/$name $url
fi
if [ -e $iso_folder/$name ] && [ -s $iso_folder/$name ]  && [ ! -e $mountPoint/iso/$name ]; then
  cp $iso_folder/$name $mountPoint/iso
fi
  
cat <<EOF >> $mountPoint/boot/grub/grub.cfg

submenu "LMDE4 ->" {
  menuentry 'Mac-azerty' {
    set isofile="/iso/$name"
    echo 'prepare loopback'
    loopback loop \$isofile
    echo 'Loading Linux (vmlinuz)'
    linux (loop)/live/vmlinuz boot=live findiso=\$isofile locales=fr_FR.UTF-8 keyboard-layouts=fr keyboard-variants=mac keyboard-options=lv3:switch,compose:lwin
    echo 'Loading initial ramdisk ...'
    initrd (loop)/live/initrd.lz
  }

  menuentry 'PC-azerty' {
    set isofile="/iso/$name"
    echo 'prepare loopback'
    loopback loop \$isofile
    echo 'Loading Linux (vmlinuz)'
    linux (loop)/live/vmlinuz boot=live findiso=\$isofile locales=fr_FR.UTF-8 keyboard-layouts=fr
    echo 'Loading initial ramdisk ...'
    initrd (loop)/live/initrd.lz
  }

  menuentry 'PC-qwerty' {
    set isofile="/iso/$name"
    echo 'prepare loopback'
    loopback loop \$isofile
    echo 'Loading Linux (vmlinuz)'
    linux (loop)/live/vmlinuz boot=live findiso=\$isofile locales=fr_FR.UTF-8
    echo 'Loading initial ramdisk ...'
    initrd (loop)/live/initrd.lz
  }
}
EOF

