
iso_folder=$1
mountPoint=$2

name="gparted-live-1.2.0-1-amd64.iso"
url=https://downloads.sourceforge.net/gparted/$name

if [ ! -e $iso_folder/$name ] ; then
  wget -O $iso_folder/$name $url
fi
if [ -e $iso_folder/$name ] && [ -s $iso_folder/$name ] && [ ! -e $mountPoint/iso/$name ] ; then
  cp $iso_folder/$name $mountPoint/iso
fi
cat <<EOF >> $mountPoint/boot/grub/grub.cfg

menuentry 'Gparted' {
  set isofile="/iso/$name"
  echo 'prepare loopback'
  loopback loop \$isofile
  echo 'Loading Linux (vmlinuz)'
  linux (loop)/live/vmlinuz boot=live findiso=\$isofile union=overlay username=user components noswap noeject vga=788 locales=fr_FR.UTF-8 keyboard-layouts=fr
  echo 'Loading initial ramdisk ...'
  initrd (loop)/live/initrd.img
}
EOF
