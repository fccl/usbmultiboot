
iso_folder=$1
mountPoint=$2

name=tetris.efi
url=https://github.com/tsani/tetrefis/releases/download/v1.0/$name
# https://github.com/tsani/tetrefis
#https://unix.stackexchange.com/questions/277373/is-it-possible-to-use-grub-to-run-an-efi-binary

if [ ! -e $iso_folder/$name ] ; then
  wget -O $iso_folder/$name $url
fi
if [ -e $iso_folder/$name ] ; then
  cp $iso_folder/tetris.efi $mountPoint/EFI/BOOT/
  cat <<EOF >> $mountPoint/boot/grub/grub.cfg
menuentry 'Tetris EFI - Marche pas sur Mac' {
  chainloader /EFI/BOOT/$name
}
EOF
fi