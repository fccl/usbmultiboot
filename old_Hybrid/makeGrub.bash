#!/bin/bash

if [ $# -lt 2 ] ; then
  iso_folder=ISOs
  mountPoint=test_usbKey
  rm $mountPoint/boot/grub/grub.cfg
else
  iso_folder=$1
  mountPoint=$2
fi

mkdir -p $mountPoint/boot/grub/
mkdir -p $mountPoint/iso/
mkdir -p $mountPoint/EFI/BOOT/
mkdir -p $iso_folder

for l in $(ls)
do
  if [[ $l == *.grub.sh ]] ; then
    chmod +x $l
    echo $l
    ./$l $iso_folder $mountPoint
  fi
done
