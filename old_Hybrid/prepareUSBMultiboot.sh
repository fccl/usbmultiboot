#!/bin/bash
usage() {
  echo "usage : ./prepareUSBMultiBoot.sh deviceX"
  echo "deviceX est le device (/dev/deviceX) pointant la cléUSB. Il est obtenu en regardant le résultat de fdisk -l (linux) ou diskutil list (OSX) : "
}

listDevice() {
  if [ "$(uname)" = "Darwin" ] ; then
    diskutil list
    #ioreg -l -p IODeviceTree | grep firmware pour savoir si on a un mac 32 ou 64bit
  else
    #fdisk -l
    lsblk
  fi
  #mount
  #df -h
}

# on s'assure d'avoir au mois un paramettre : le Device
if [ $# -lt 1 ] ; then
  listDevice
  usage
  exit 1
fi

#on s'assure d'être en mode administrateur.
if [ 'root' != $USER ] ; then
  echo "le script doit avoir les droits root"
  usage
  exit 1
fi

#N° de device à formater (ex: /dev/sdb linux ou /dev/disk2 osx)
device=$1
#récupère les point de montage des partition du device
list=$(mount | grep $device | cut -d " " -f 3)
#Démonte les partitions du device
for l in $list
do
  umount $l
done
echo "-------- Nettoyage de la clé -----------"
wipefs -a $device && sgdisk -Z $device
echo "-------- Table de partition GPT et Partition ESP en 1 -----------"
parted $device -s mktable gpt mkpart ESP fat32 2MiB 100% set 1 ESP on
echo "-------- Vérification de la partition -----------"
parted $device print | grep ESP
uuid=$(blkid | grep "$device")
echo $uuid
echo "-------- Formatage en vFat -----------"
mkfs.vfat -F 32 -n ESP ${device}1
echo "-------- veréificatio formatage avec lsblk -----------"
lsblk -n -o FSTYPE ${device}1
parted $device print
expected=': gpt'
tbPart=$(parted $device print | grep "$expected")
if [[ $tbPart =~ '*: gpt' ]] ; then
  echo $tbPart
  echo "il y a un problème sur la vérification de la Table de partition -----------"
  exit 1
fi
mountPoint=usbKey
echo "-------- montage de la partition dans '$mountPoint' -----------"
rm -rf $mountPoint
mkdir $mountPoint
mount ${device}1 $mountPoint
echo "-------- installation de grub -----------"
grub-install --version
grub-install --removable --efi-directory=$mountPoint --boot-directory=$mountPoint/boot --removable --force --locales="fr" ${device}
rm $mountPoint/EFI/BOOT/BOOTX64.EFI $mountPoint/EFI/BOOT/BOOTX64.CSV
mv $mountPoint/EFI/BOOT/grubx64.efi $mountPoint/EFI/BOOT/BOOTX64.EFI
echo "-------- Recupération et copies des ISOs -----------"
./updateMenuGrub.sh $device
