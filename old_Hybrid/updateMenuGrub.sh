#!/bin/bash
usage() {
  echo "usage : ./prepareUSBMultiBoot.sh deviceX"
  echo "deviceX est le device (/dev/deviceX) pointant la cléUSB. Il est obtenu en regardant le résultat de fdisk -l (linux) ou diskutil list (OSX) : "
}

listDevice() {
  if [ "$(uname)" = "Darwin" ] ; then
    diskutil list
    #ioreg -l -p IODeviceTree | grep firmware pour savoir si on a un mac 32 ou 64bit
  else
    #fdisk -l
    lsblk
  fi
  #mount
  #df -h
}

# on s'assure d'avoir au mois un paramettre : le Device
if [ $# -lt 1 ] ; then
  listDevice
  usage
  exit 1
fi

#on s'assure d'être en mode administrateur.
if [ 'root' != $USER ] ; then
  echo "le script doit avoir les droits root"
  usage
  exit 1
fi

echo "-------- montage de la partition dans '$mountPoint' -----------"
#N° de device à formater (ex: /dev/sdb linux ou /dev/disk2 osx)
device=$1
mountPoint=usbKey
rm -rf $mountPoint
mkdir $mountPoint
mount ${device}1 $mountPoint

echo "-------- Recupération et copies des ISOs -----------"
iso_folder="ISOs"
./makeGrub.bash $iso_folder $mountPoint

sync
echo "-------- Détail du grub.cfg pour (et à coté de) grubx64.efi -----------"
cat "$mountPoint/EFI/BOOT/grub.cfg"
echo "-------- Détail du grub.cfg pour le menu -----------"
cat "$mountPoint/boot/grub/grub.cfg"
echo "-------- Démontage de la clé -----------"
umount $mountPoint
echo "-------- Vérification de la partition -----------"
parted $device print | grep ESP
blkid | grep "$device"
